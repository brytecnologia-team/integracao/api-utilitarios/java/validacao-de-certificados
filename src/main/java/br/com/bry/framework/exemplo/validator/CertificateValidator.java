package br.com.bry.framework.exemplo.validator;

import java.io.File;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import br.com.bry.framework.exemplo.config.CertificateConfig;
import br.com.bry.framework.exemplo.config.ServiceConfig;
import br.com.bry.framework.exemplo.util.ConverterUtil;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.specification.RequestSpecification;

public class CertificateValidator {

	private String token = ServiceConfig.ACCESS_TOKEN;

	private static final String URL_VALIDATOR = ServiceConfig.URL_VALIDATOR;

	private ConverterUtil converterUtil = new ConverterUtil();

	/**
	 * Validates the certificate
	 */
	public void validateCertificate() {
		try {

			// Validates the access token
			this.validateAccessToken();

			// Configures the request
			RequestSpecification requestSpec = this.createValidatorRequest();

			// Perform communication with the API
			Object validatorResponse = this.connectToService(requestSpec);

			// Prints certificate report
			this.printValidationInformation(validatorResponse);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * Validates the configured access token
	 * 
	 * @throws Exception
	 */
	private void validateAccessToken() throws Exception {
		if (this.token.equals("<INSERT_VALID_ACCESS_TOKEN>")) {
			throw new Exception("Set up a valid token!");
		}
	}

	/**
	 * Configures the certificate request
	 * 
	 * @return Certificate request
	 * @throws Exception
	 */
	private RequestSpecification createValidatorRequest() throws Exception {
		String filePath = CertificateConfig.CERTIFICATE_PATH;

		RequestSpecBuilder builder = new RequestSpecBuilder();

		int certificateQuantitiesToValidate = 1;

		for (int indexOfCertificate = 0; indexOfCertificate < certificateQuantitiesToValidate; indexOfCertificate++) {
			builder.addMultiPart("certificates[" + indexOfCertificate + "][nonce]",
					Integer.toString(indexOfCertificate));
			builder.addMultiPart("certificates[" + indexOfCertificate + "][content]", new File(filePath));
		}

		builder.addMultiPart("nonce", CertificateConfig.NONCE);
		builder.addMultiPart("mode", CertificateConfig.MODE);
		builder.addMultiPart("extensionsReturn", Boolean.toString(CertificateConfig.EXTENSIONS_RETURN));
		builder.addMultiPart("contentReturn", Boolean.toString(CertificateConfig.CONTENTS_RETURN));

		return builder.build();

	}

	/**
	 * Perform communication with the API to validate the certificate
	 * 
	 * @param requestSpec
	 * @return Certificate object answer
	 * @throws Exception
	 */
	private Object connectToService(RequestSpecification requestSpec) throws Exception {
		return RestAssured.given().auth().preemptive().oauth2(token).spec(requestSpec).expect().statusCode(200).when()
				.post(URL_VALIDATOR).as(Object.class);
	}

	/**
	 * Prints the certificate report
	 * 
	 * @param validatorResponse
	 * @throws JSONException
	 */
	private void printValidationInformation(Object validatorResponse) throws JSONException {
		String jsonResponse = this.converterUtil.convertObjectToJSON(validatorResponse);

		System.out.println("Validator JSON response: " + jsonResponse);

		JSONObject jsonObject = new JSONObject(jsonResponse);

		JSONArray reports = jsonObject.getJSONArray("reports");

		String reportsStringJson = reports.getString(0);

		JSONObject reportsJsonObject = new JSONObject(reportsStringJson);

		JSONObject chainStatus = new JSONObject(reportsJsonObject.getString("status"));

		JSONArray certificateStatusList = chainStatus.getJSONArray("certificateStatusList");

		int indexOfCertificate = certificateStatusList.length() - 1;
		JSONObject certificateOfSubject = certificateStatusList.getJSONObject(indexOfCertificate);

		System.out.println("Subject information: ");
		if (chainStatus != null && certificateOfSubject != null) {
			JSONObject certificateInfo = certificateOfSubject.getJSONObject("certificateInfo");
			JSONObject subjectDN = certificateInfo.getJSONObject("subjectDN");
			System.out.println("Name: " + subjectDN.getString("cn"));
			System.out.println("General status of the certificate chain: " + chainStatus.getString("status"));
			System.out.println("Certificate status: " + certificateOfSubject.getString("status"));
			if (certificateOfSubject.getString("status").equals("INVALID")) {
				System.out.println(certificateOfSubject.getString("errorStatus"));
				String revocationStatus = null;
				try {
					revocationStatus = certificateOfSubject.getString("revocationStatus");
				} catch (JSONException jsonException) {
					System.out.println("No encontrada a chave: revocationStatus");
				}

				if (revocationStatus != null && !revocationStatus.isEmpty()) {
					System.out.println(revocationStatus);
				}

			}
			System.out.println("ICP-Brazil certificate: " + certificateOfSubject.getString("pkiBrazil"));
			System.out.println("Initial validity date of certificate: "
					+ certificateInfo.getJSONObject("validity").getString("notBefore"));
			System.out.println("End date of validity of certificate: "
					+ certificateInfo.getJSONObject("validity").getString("notAfter"));
		} else {
			System.out.println("Incomplete chain of the subject: The certificate could not be verified");
			System.out.println("General status of chain: " + chainStatus.getString("status"));
		}
	}

}
