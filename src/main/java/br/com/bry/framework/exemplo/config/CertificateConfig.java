package br.com.bry.framework.exemplo.config;

import java.io.File;

public class CertificateConfig {

	private static final String PATH_SEPARATOR = File.separator;

	// Request identifier
	public static final String NONCE = "1";

	// Available values: 'CHAIN' = Report with certificate chain information and
	// 'BASIC' = Report without the certificate chain information.
	public static final String MODE = "CHAIN";

	// Available values: 'true' = Report with signature timestamp information and
	// 'false' = Report without signature timestamp information
	public static final boolean CONTENTS_RETURN = true;

	// Available values: 'true' = Report with extension information, such as:
	// authority key identifier, CRL distribution points, birth data and 'false' =
	// Report without extension information
	public static final boolean EXTENSIONS_RETURN = true;

	// location where the certificate is stored
	public static final String CERTIFICATE_PATH = System.getProperty("user.dir") + "" + PATH_SEPARATOR + "src"
			+ PATH_SEPARATOR + "main" + PATH_SEPARATOR + "resources" + PATH_SEPARATOR + "certificado" + PATH_SEPARATOR
			+ "user-test.cer";

}
