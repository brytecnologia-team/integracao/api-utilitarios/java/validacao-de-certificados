package br.com.bry.framework.exemplo;

import br.com.bry.framework.exemplo.validator.CertificateValidator;

public class Application {

	/**
	 * Run the application
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		CertificateValidator certificateValidator = new CertificateValidator();

		System.out.println(
				"=====================================Starting certificate verification ...=====================================");

		// The following method will validate the certificate
		certificateValidator.validateCertificate();
	}

}
