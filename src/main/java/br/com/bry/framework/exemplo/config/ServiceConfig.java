package br.com.bry.framework.exemplo.config;

public class ServiceConfig {

	public static final String URL_VALIDATOR = "https://fw2.bry.com.br/api/validador-service/v1/certificateReports";
	//https://URL_HUB_SIGNER/api/validador-service/v1/certificateReports
	
	public static final String ACCESS_TOKEN = "<INSERT_VALID_ACCESS_TOKEN>";

}
